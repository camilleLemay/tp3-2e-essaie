document.addEventListener("DOMContentLoaded", function(event) {

    //Un commentaire
    console.log("Ça fonctionne");

    //==============================INSCRIPTION==============================\\

    document.getElementById("inscription-lien").addEventListener("click",function (e) {

        e.preventDefault();

        document.querySelector("#inscription-modale").classList.add("open");

    });

    document.getElementById("fermer-inscription-modale").addEventListener("click",function (e) {

        e.preventDefault();

        document.querySelector("#inscription-modale").classList.remove("open");

    });


    //==============================CONNEXION==============================\\

    document.getElementById("connexion-lien").addEventListener("click",function (e) {

        e.preventDefault();

        document.querySelector("#connexion-modale").classList.add("open");

    });

    document.getElementById("fermer-connexion-modale").addEventListener("click",function (e) {

        e.preventDefault();

        document.querySelector("#connexion-modale").classList.remove("open");

    });


    //POUR SCROLL EN HAUT DE LA PAGE

    /*function topFunction() {
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    }

    document.getElementById("bouton-footer").addEventListener("click", topFunction);*/

});